FROM amazoncorretto:17-alpine-jdk

COPY target/RestServiceDemo*.jar app.jar

EXPOSE 8080

ENTRYPOINT ["java", "-jar"]

CMD ["app.jar"]

