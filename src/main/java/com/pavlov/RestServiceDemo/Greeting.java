package com.pavlov.RestServiceDemo;

public record Greeting(long id, String content) {
}
